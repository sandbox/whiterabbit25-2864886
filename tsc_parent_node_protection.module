<?php
/**
 * Created by PhpStorm.
 * User: rabbi
 * Date: 3/2/2017
 * Time: 10:26 PM
 */

// Add settings to field config form
function tsc_parent_node_protection_form_field_ui_field_edit_form_alter(&$form, &$form_state, $form_id)
{
    // dpm($form);

    // Only show for entity reference fields
    if ($form['#field']['type'] == 'entityreference') {
        $form['parent-node-protection-settings'] = array(
            '#type' => 'fieldset',
            '#title' => 'Parent Node Protection Settings',
            '#tree' => TRUE
        );

        // Checkbox for whether to protect nodes with children
        $form['parent-node-protection-settings']['parent-protection-bool'] = array(
            '#type' => 'checkbox',
            '#title' => 'Disable node deletion if child nodes exist'
        );

        $field_name = $form['#field']['field_name'];
        $child_type = $form['#instance']['bundle'];
        $parent_types = $form['#field']['settings']['handler_settings']['target_bundles'];

        $query = db_select('tsc_parent_node_protection', 'p');
        $query->addField('p', 'parent_protection_bool');
        $query->condition('p.field_name', $field_name);
        $query->condition('p.child_type', $child_type);
        $parent_protection = $query->execute()->fetchField();

        $form['parent-node-protection-settings']['parent-protection-bool']['#default_value'] = $parent_protection;

        //Add custom submit handler
        $form['#submit'][] = 'tsc_protect_parent_nodes_field_ui_field_edit_form_submit';
    }
}

// Process function for module settings
function tsc_protect_parent_nodes_field_ui_field_edit_form_submit($form, &$form_state) {
    // Gather necessary info from form state
    $field_name = $form['#field']['field_name'];
    $parent_protection_input = $form_state['input']['parent-node-protection-settings']['parent-protection-bool'] ? 1 : 0;
    $child_type = $form['#instance']['bundle'];
    $parent_types = $form['#field']['settings']['handler_settings']['target_bundles'];

    foreach ($parent_types as $parent_type) {// Get id of module settings row
        $query = db_select('tsc_parent_node_protection', 'p');
        $query->addField('p', 'id');
        $query->condition('p.field_name', $field_name);
        $query->condition('p.child_type', $child_type);
        $query->condition('p.parent_type', $parent_type);
        $row_id = $query->execute()->fetchField();

        // If settings logged for this field instance update row, otherwise create record
        if ($row_id) {
            $updated_row = db_update('tsc_parent_node_protection')
                ->fields(array(
                    'parent_protection_bool' => $parent_protection_input
                ))
                ->condition('id', $row_id)
                ->execute();

            // dpm('Row updated');

        } else {
            $new_row = db_insert('tsc_parent_node_protection')
                ->fields(array(
                    'field_name' => $field_name,
                    'parent_protection_bool' => $parent_protection_input,
                    'parent_type' => $parent_type,
                    'child_type' => $child_type
                ))
                ->execute();

            // dpm('Row added');
        }
    }
}

/**
 * Implements hook_node_access().
 */
function tsc_parent_node_protection_node_access($node, $op, $account)
{
    if ($op == 'delete') {
        $node_type = $node->type;
        $nid = $node->nid;

        return tsc_parent_node_protection_child_node_check($nid, $node_type) ? NODE_ACCESS_ALLOW : NODE_ACCESS_DENY;
    }
}

/**
 * Implements hook_node_access().
 */
/**
 * Implements hook_node_view().
 */
function tsc_parent_node_protection_node_view($node, $view_mode, $langcode)
{
    $node_type = $node->type;
    $nid = $node->nid;

    $query = db_select('tsc_parent_node_protection', 'p');
    $query->addField('p', 'child_type');
    $query->addField('p', 'field_name');
    $query->condition('p.parent_type', $node_type);
    $results = $query->execute()->fetchAll();

    if(!empty($results)) {
        $delete_allowed = true;
        $child_type_array = array();
        foreach($results as $result) $child_type_array[$result->field_name][] = $result->child_type;
        dpm($child_type_array);

        foreach ($child_type_array as $field => $child_types) {
            $field_info = field_info_field($field);

            $table_name = array_keys($field_info['storage']['details']['sql']['FIELD_LOAD_CURRENT'])[0];
            $column_key = array_keys($field_info['storage']['details']['sql']['FIELD_LOAD_CURRENT'][$table_name])[0];
            $column_name = $field_info['storage']['details']['sql']['FIELD_LOAD_CURRENT'][$table_name][$column_key];

            dpm($table_name . ': ' . $column_name);

            $query = db_select('node', 'n');
            $query->leftJoin($table_name, $table_name, 'n.nid = ' . $table_name . '.entity_id');
            $query->addField('n', 'nid');
            $query->condition($table_name . '.' . $column_name, $nid);

            $types_or = db_or();
            foreach($child_types as $child_type) {
                $types_or->condition('n.type', $child_type);
            }

            $child_nodes = $query->execute()->fetchAll();
            dpm($child_nodes);

            if(!empty($child_nodes)) $delete_allowed = false;
            dpm($delete_allowed ? 'Delete' : 'No delete');
        }
    }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function tsc_parent_node_protection_form_node_delete_confirm_alter(&$form, &$form_state, $form_id)
{

    $nid = $form['#node']->nid;
    $node_type = $form['#node']->type;

    $childless = tsc_parent_node_protection_child_node_check($nid, $node_type);

    if(!$childless) {
        $form['node_child_warning'] = array(
            '#type' => 'markup',
            '#markup' => $form['#node']->title . ' is being referenced elsewhere on the site!',
            '#prefix' => '<h1>',
            '#suffix' => '</h1>'
        );
    }
    // dpm($form);
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function tsc_parent_node_protection_form_node_admin_content_alter(&$form, &$form_state, $form_id)
{
    if(isset($form['operation']['#value']) && $form['operation']['#value'] == 'delete') {

        foreach($form['nodes'] as $index => $node) {
            if(is_numeric($index)) {
                $query = db_select('node', 'n');
                $query->addField('n', 'type');
                $query->addField('n', 'title');
                $query->condition('n.nid', $index);
                $results = $query->execute()->fetchAll()[0];

                $node_title = $results->title;
                $node_type = $results->type;

                $childless = tsc_parent_node_protection_child_node_check($index, $node_type);

                if(!$childless) {
                    $form[$node_title . ' warning'] = array(
                        '#type' => 'markup',
                        '#markup' => $node_title . ' is being referenced elsewhere on the site!',
                        '#prefix' => '<h1>',
                        '#suffix' => '</h1>'
                    );
                }
            }
        }
    }
}

function tsc_parent_node_protection_child_node_check($nid, $node_type) {
    $query = db_select('tsc_parent_node_protection', 'p');
    $query->addField('p', 'child_type');
    $query->addField('p', 'field_name');
    $query->condition('p.parent_type', $node_type);
    $results = $query->execute()->fetchAll();

    if (!empty($results)) {
        $child_type_array = array();
        foreach ($results as $result) $child_type_array[$result->field_name][] = $result->child_type;

        foreach ($child_type_array as $field => $child_types) {
            $field_info = field_info_field($field);

            $table_name = array_keys($field_info['storage']['details']['sql']['FIELD_LOAD_CURRENT'])[0];
            $column_key = array_keys($field_info['storage']['details']['sql']['FIELD_LOAD_CURRENT'][$table_name])[0];
            $column_name = $field_info['storage']['details']['sql']['FIELD_LOAD_CURRENT'][$table_name][$column_key];

            $query = db_select('node', 'n');
            $query->leftJoin($table_name, $table_name, 'n.nid = ' . $table_name . '.entity_id');
            $query->addField('n', 'nid');
            $query->condition($table_name . '.' . $column_name, $nid);

            $types_or = db_or();
            foreach ($child_types as $child_type) {
                $types_or->condition('n.type', $child_type);
            }
            $query->condition($types_or);

            $child_nodes = $query->execute()->fetchAll();

            if (!empty($child_nodes)) return false;
        }
    }
    return true;
}